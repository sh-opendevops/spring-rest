FROM openjdk:8-jdk-alpine

ADD ./app/target/spring-rest-app-1.0.0.jar /app/app.jar

ENTRYPOINT ["java","-jar","/app/app.jar"]
